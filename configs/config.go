	package configs

type Config struct {
	ApiKeyRebrand string `mapstructure:"API_KEY_REBRAND1"`
	Filename      string `mapstructure:"FILENAME1"`
	Sheet      string `mapstructure:"SHEET1"`
	Title      string `mapstructure:"TITLE"`
	UrlBackendBriguna      string `mapstructure:"URL_BACKEND_BRIGUNA"`
	UrlEsb      string `mapstructure:"URL_ESB"`
	NumberOfData      string `mapstructure:"NUMBER_OF_DATA"`
}

var Configs *Config

func LoadConfig(path string) (err error) {

	
	conf :=Config{
		ApiKeyRebrand:     "d9bd0de389e14c47a5cb821e0c14bbd2",
		Filename:          "20220331.xlsx",
		Sheet:             "Sheet1",
		Title:             "Briguna",
		UrlBackendBriguna: "https://api.briguna.bri.co.id",
		UrlEsb:            "http://esb.bri.co.id:6001/NewBriInterface4Gen.asmx/doMbaseTransaction",
		NumberOfData:      "1000",
	}

	Configs = &conf

	return
}


