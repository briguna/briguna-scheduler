package outbound

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"time"
)

func Post(request *http.Request) (response *http.Response, err error) {

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: false},
	}

	client := &http.Client{
		Timeout:   80 * time.Second,
		Transport: transport,
	}

	response, err = client.Do(request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode > 299 {
		rb := CopyHttpResponsesBody(response)
		log.Println(rb)
		return response, fmt.Errorf("server request rejected because: %s", response.Status)
	}

	return response, nil
}
