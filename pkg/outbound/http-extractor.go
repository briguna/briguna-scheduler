package outbound

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func CopyHttpRequestBody(req *http.Request) string {
	var requestBody *http.Request
	if req.Body != nil {
		requestBodyJson, _ := ioutil.ReadAll(req.Body)
		// don't close the body, because it's a copy of the original response
		_ = json.Unmarshal(requestBodyJson, &requestBody)
		req.Body = ioutil.NopCloser(bytes.NewReader(requestBodyJson))
		return string(requestBodyJson)
	}
	return ""
}

func CopyHttpResponsesBody(req *http.Response) string {
	var responseBody *http.Response
	if req.Body != nil {
		requestBodyJson, _ := ioutil.ReadAll(req.Body)
		// don't close the body, because it's a copy of the original response
		_ = json.Unmarshal(requestBodyJson, &responseBody)
		req.Body = ioutil.NopCloser(bytes.NewReader(requestBodyJson))
		return string(requestBodyJson)
	}
	return ""
}
