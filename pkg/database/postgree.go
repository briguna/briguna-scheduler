package database

import (
	"bitbucket.org/briguna/briguna-scheduler/pkg"
	"bitbucket.org/briguna/briguna-scheduler/pkg/logger"
	"context"
	"github.com/jinzhu/gorm"
	"time"
)

var DbGorm *gorm.DB

func InitGorm(ctx context.Context) {
	connectionString := pkg.Getenv("GORM_CONNECTION")
	db, err := gorm.Open("postgres", connectionString)
	if err != nil || db.Error != nil {
		return
	}

	sqlDB := db.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	if err != nil || db.Error != nil {
		return
	}
	logger.GetLogger("db","init connection").Error(err)

	DbGorm = db
}
