package logger

import (
	"bitbucket.org/briguna/briguna-scheduler/pkg"
	"context"
	"github.com/sirupsen/logrus"
	"strings"
)

var log *logrus.Entry

func Configure() {
	format := pkg.Getenv("LOG_FORMAT")
	switch format {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	default:
		logrus.SetFormatter(&logrus.TextFormatter{})
	}

	lvl := strings.ToLower(pkg.Getenv("LOG_LEVEL"))
	switch lvl {
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
	}

	log = logrus.WithField("app", "briguna-scheduler")
}

func setDefault() {
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	log = logrus.WithField("app", "briguna-scheduler")
}

func GetLogger(pkg, funcName string) *logrus.Entry {
	if log == nil {
		setDefault()
	}
	return log.WithFields(logrus.Fields{
		"function": funcName,
		"package":  pkg,
	})
}

func GetLoggerContext(ctx context.Context, pkg, funcName string) *logrus.Entry {
	if log == nil {
		setDefault()
	}
	return log.WithContext(ctx).WithFields(logrus.Fields{
		"function": funcName,
		"package":  pkg,
	})
}
