package rest

import (
	"crypto/tls"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
	"time"
)


type Method string

const (
	GET    Method = "GET"
	POST   Method = "POST"
	PUT    Method = "PUT"
	PATCH  Method = "PATCH"
	DELETE Method = "DELETE"
)

type Request struct {
	Method      Method
	URL         string
	Headers     map[string]string
	QueryParams map[string]string
	Body        []byte
}

func SendRequestForm(request Request) (*http.Response, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   150 * time.Second,
		Transport: tr,
	}
	if len(request.QueryParams) != 0 {
		request.URL += "?"
		params := url.Values{}
		queryParams := request.QueryParams
		for key, value := range queryParams {
			params.Add(key, value)
		}

		request.URL = request.URL + params.Encode()
	}
	var requestBody map[string]string
	if err := json.Unmarshal(request.Body, &requestBody); err != nil {
		return nil, err
	}

	data := url.Values{}
	for k, v := range requestBody {
		data.Set(k, v)
	}
	req, err := http.NewRequest(string(request.Method), request.URL, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}

	headers := request.Headers
	if len(request.Headers) == 0 {
		return nil, err
	} else {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func SendRequestRaw(request Request) (*http.Response, error) {
	requestBody := strings.NewReader(string(request.Body))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   150 * time.Second,
		Transport: tr,
	}
	if len(request.QueryParams) != 0 {
		request.URL += "?"
		params := url.Values{}
		queryParams := request.QueryParams
		for key, value := range queryParams {
			params.Add(key, value)
		}

		request.URL = request.URL + params.Encode()
	}

	req, err := http.NewRequest(string(request.Method), request.URL, requestBody)
	if err != nil {
		return nil, err
	}

	// default
	req.Header.Add("Content-Type", "application/json")

	if len(request.Headers) != 0 {
		headers := request.Headers
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func CustomizeSendRequestRaw(request Request) (*http.Response, error) {
	requestBody := strings.NewReader(string(request.Body))

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   150 * time.Second,
		Transport: tr,
	}
	if len(request.QueryParams) != 0 {
		request.URL += "?"
		params := url.Values{}
		queryParams := request.QueryParams
		for key, value := range queryParams {
			params.Add(key, value)
		}

		request.URL = request.URL + params.Encode()
	}

	req, err := http.NewRequest(string(request.Method), request.URL, requestBody)
	if err != nil {
		return nil, err
	}


	if len(request.Headers) != 0 {
		headers := request.Headers
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}


	res, err := client.Do(req)
	if err != nil {
		return nil, nil
	}

	return res, nil
}

