# Briguna backend service

> Proposed grpc gateway based service .This project is still in development stage, any critic and suggestion all very based including but not limited to project name, function naming, folder structure etc. please refer to CONTRIBUTING.md.

## Prerequisite

- Install [go](https://golang.org/doc/install) in local machine for convenience development experience (auto complete, code sugestion, etc)
- Install golang plugin to your editor choice (ie. VSCode, Atom, Goland, Intellij IDE)
- [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/)
- Protoc and proto-gen-go plugin (described in the next session)

## How to run

Despite, it is possible to run this project in local machine Please follow this steps:
- Changes file .env to your config `.cp env.dev to .env`.
- Run apps gp to root project `go run main.go`.



##  Folder structure.
    .
    ├── cmd                     # File for define handler endpoint
    ├── config                  # Define connection to database,redis,minio, grpc
    ├── docs                 # Define general function that use for handler
    ├── examples              # Define interface contract that using for repository and business
    ├── internal                   # Define for struct that using general
    ├── pkg                   # Define rpc proto function, request and response
    ├── scripts                # Define function that using for call surrounding system
    ├── test       # Folder for template document  
 




## Deployment config files
### *Environment Variables (Development)*
| Name | Value | Description | Location |
| ------ | ------ | ------ | ------ |
| GORM_CONNECTION | host=host=172.18.98.59 port=32712 user=postgres dbname=briguna_backup_prod sslmode=disable password=P@ssw0rd|	orm connection to db | OS |
| GORM_LOG | true | log orm| secret file|
| SECRET_KEY| 123456 | scret  key | secret file |
| SECRET_KEY_OTP |	AAAABBBBCCCCDDDD | screet key otp | secret file |
| MERCHANT_KEY | hSmmyXqkgKGCw2TM7TUW | key for marchent| secret file |


### *Environment Variables (Production)*
| Name | Value | Description | Location |
| ------ | ------ | ------ | ------ |
| GORM_CONNECTION | host=postgres-ceria-modalkerja-postgresql port=5432 user=postgres dbname=ceria_modalkerja sslmode=disable password=P@ssw0rd|	orm connection to db | OS |
| GORM_LOG | true | log orm| secret file|
| SECRET_KEY| 123456 | scret  key | secret file |
| SECRET_KEY_OTP |	AAAABBBBCCCCDDDD | screet key otp | secret file |
| MERCHANT_KEY | hSmmyXqkgKGCw2TM7TUW | key for marchent| secret file |
