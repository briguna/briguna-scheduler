package repository

import (
	"bitbucket.org/briguna/briguna-scheduler/configs"
	"bitbucket.org/briguna/briguna-scheduler/internal/domain"
	"bitbucket.org/briguna/briguna-scheduler/pkg/outbound"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type TokenRepository interface {
	GetLoginToken(noHP string) (string, error)
}

func GetLoginToken(noHP string)  (*domain.TokenData, error){
	ccs := map[string]string{
		"noHp":noHP,
	}

	tok:=&domain.TokenData{
		AccessToken: "a",
		URI:         "a",
	}

	reqJson, _ := json.Marshal(ccs)
	httpParameters,err := http.NewRequest(http.MethodPost,configs.Configs.UrlBackendBriguna+"/v1/auth/new_login",bytes.NewBuffer(reqJson))
	if err != nil {
		return tok, fmt.Errorf("error while build request parameters for apigee: %s", err.Error())
	}

	httpParameters.Header.Add("Content-Type", "application/json")
	httpParameters.Header.Add("grpc-metadata-client", "BR1M0")
	httpParameters.Header.Add("grpc-metadata-secret", "Brigun@XBr1m0")
	responses, err := outbound.Post(httpParameters)
	if err != nil {
		return tok, fmt.Errorf("error while request http connection: %s", err.Error())
	}
	buffResponses, _ := ioutil.ReadAll(responses.Body)
	defer responses.Body.Close()
	var typeResponses domain.TokenResponse
	err = json.Unmarshal(buffResponses, &typeResponses)

	if err != nil {
		return tok, fmt.Errorf("error while translate response: %s", err.Error())
	}

	return &typeResponses.Contents, nil
}