package repository

import (
	"bitbucket.org/briguna/briguna-scheduler/configs"
	"bitbucket.org/briguna/briguna-scheduler/internal/domain"
	"bitbucket.org/briguna/briguna-scheduler/pkg/outbound"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type RebrandRepository interface {
	CreateShortLink(url string,phone string) (string,string, error)
	DeleteShortLink(id string) (string, error)
}

func CreateShortLink(url string,phone string) (string,string, error)  {
	request := map[string]string{
		"title": configs.Configs.ApiKeyRebrand,
		"slashtag": "briguna_"+phone,
		"destination": url,
	}

	reqJson, _ := json.Marshal(request)
	httpParameters,err := http.NewRequest(http.MethodPost,"https://api.rebrandly.com/v1/links", bytes.NewBuffer(reqJson))
	if err != nil {
		return "","", fmt.Errorf("error while build request : %s", err.Error())
	}

	httpParameters.Header.Add("Content-Type", "application/json")
	httpParameters.Header.Add("apikey", configs.Configs.ApiKeyRebrand)

	responses, err := outbound.Post(httpParameters)
	if err != nil {
		return "-","-", fmt.Errorf("error while request http connection: %s", err.Error())
	}
	buffResponses, _ := ioutil.ReadAll(responses.Body)
	defer responses.Body.Close()
	var response domain.RebrandResponses
	err = json.Unmarshal(buffResponses, &response)

	if err != nil {
		return "","", fmt.Errorf("error while translate response: %s", err.Error())
	}


	return response.ShortUrl,response.Id, nil
}

func DeleteShortLink(id string) (string, error)  {

	request := map[string]string{
		"id": id,
	}

	reqJson, _ := json.Marshal(request)
	httpParameters,err := http.NewRequest(http.MethodDelete,"https://api.rebrandly.com/v1/links/"+id, bytes.NewBuffer(reqJson))
	if err != nil {
		return "", fmt.Errorf("error while build request : %s", err.Error())
	}

	httpParameters.Header.Add("Content-Type", "application/json")
	httpParameters.Header.Add("apikey", configs.Configs.ApiKeyRebrand)

	responses, err := outbound.Post(httpParameters)
	if err != nil {
		return "", fmt.Errorf("error while request http connection: %s", err.Error())
	}
	buffResponses, _ := ioutil.ReadAll(responses.Body)
	defer responses.Body.Close()
	var response domain.RebrandResponses
	err = json.Unmarshal(buffResponses, &response)

	if err != nil {
		return "", fmt.Errorf("error while translate response: %s", err.Error())
	}

	return "response.ShortUrl", nil
}