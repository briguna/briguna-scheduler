package repository

type BrinetRepository interface {
	CheckOutStanding(string) (string, error)
	InquriryTagihan(string) (string, error)
	InquiryAft(string) (string, error)
	InquiryHold(string) (string, error)
	InquiryBrisurf(string) (string, error)
}
