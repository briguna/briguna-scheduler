package repository

import (
	"bitbucket.org/briguna/briguna-scheduler/internal/domain"
	"bitbucket.org/briguna/briguna-scheduler/pkg/outbound"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type NotificationRepository interface {
	SendSms(noHP string) (string, error)
}

func SendSms(phone string, name string, url string) (string, error) {
	request := map[string]string{
		"norek":  "0",
		"divisi": "SIT",
		"produk": "Sms Dev",
		"fitur":  "",
		"hp":     phone,
		"pesan":  "Yth "+name+"\nAnda berkesempatan mengikuti simulasi bisnis untuk top-up Briguna Kawan.Silahkan klik link berikut ini:"+url,
		"flag":   "",
	}

	reqJson, _ := json.Marshal(request)
	httpParameters,err := http.NewRequest(http.MethodPost,"http://172.18.133.198:9001/fcd/FCD_SMS", bytes.NewBuffer(reqJson))
	if err != nil {
		return "", fmt.Errorf("error while build request : %s", err.Error())
	}

	httpParameters.Header.Add("Content-Type", "application/json")
	httpParameters.Header.Add("apikey", "d9bd0de389e14c47a5cb821e0c14bbd2")

	responses, err := outbound.Post(httpParameters)
	if err != nil {
		return "", fmt.Errorf("error while request http connection: %s", err.Error())
	}
	buffResponses, _ := ioutil.ReadAll(responses.Body)
	defer responses.Body.Close()
	var response domain.NotificationResponse
	err = json.Unmarshal(buffResponses, &response)

	if err != nil {
		return "", fmt.Errorf("error while translate response: %s", err.Error())
	}

	return response.Body.StatusCode, nil
}
