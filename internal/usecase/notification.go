package usecase

import (
	"bitbucket.org/briguna/briguna-scheduler/configs"
	"bitbucket.org/briguna/briguna-scheduler/internal/repository"
	"fmt"
	"github.com/xuri/excelize/v2"
	"log"
)

type NotificationUseCase interface {
	SendSms() (string, error)
}

func SendSms() {
	xlsx, err := excelize.OpenFile("./docs/"+configs.Configs.Filename)
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	sheet1Name := configs.Configs.Sheet

	for i := 0; i < N; i++ {
		name, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("A%d", i+1))
		phone, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("B%d", i+1))
		url, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("D%d", i+1))

		response,err := repository.SendSms(phone, name, url)
		if err != nil{
			continue
		}
		log.Println("send success: ",i, response)
	}


	xlsx.Close()
}
