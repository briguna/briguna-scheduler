package usecase

import (
	"bitbucket.org/briguna/briguna-scheduler/configs"
	"bitbucket.org/briguna/briguna-scheduler/internal/repository"
	"fmt"
	"github.com/xuri/excelize/v2"
	"log"
	"strconv"
)

type RebrandUseCase interface {
	CreateShortLink()
	DeleteShortLink()
}

func CreateShortLink()  {
	xlsx, err := excelize.OpenFile("./docs/"+configs.Configs.Filename)
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	sheet1Name := "Sheet4"

	rows := make([]M, 0)
	for i := 0; i < N; i++ {
		phone, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("B%d", i+1))
		token, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("C%d", i+1))

		url,id,_ := repository.CreateShortLink(token, phone)

		row := M{
			"Name":  "" ,
			"Phone": phone,
			"Url": "",
			"Short": url,
			"Id":id,
		}
		rows = append(rows, row)
		log.Println("short success: "+ strconv.Itoa(i) + url + ":" + id)
	}

	for i := 0; i < N; i++ {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+1), rows[i]["Short"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+1), rows[i]["Id"])
	}

	xlsx.SaveAs("./docs/"+configs.Configs.Filename)
	xlsx.Close()
}

func DeleteShortLink()  {
	xlsx, err := excelize.OpenFile("./docs/"+configs.Configs.Filename)
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	sheet1Name := "Sheet1"

	rows := make([]M, 0)
	for i := 0; i < N; i++ {
		id, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("E%d", i+1))

		url,err := repository.DeleteShortLink(id)
		if err != nil{
			continue
		}

		row := M{
			"Name":  "" ,
			"Phone": "",
			"Url": "",
			"Short": url,
			"Id":id,
		}
		rows = append(rows, row)
		log.Println("delete success: ",i)
	}

	xlsx.Close()
}

