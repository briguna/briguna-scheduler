package usecase

import (
	"bitbucket.org/briguna/briguna-scheduler/configs"
	"bitbucket.org/briguna/briguna-scheduler/internal/repository"
	"fmt"
	"github.com/xuri/excelize/v2"
	"log"
)

type TokenUseCase interface {
	GetLoginToken() (string, error)
}


type M map[string]interface{}
var N int = 1000

func GetLoginToken() {
	fmt.Println(configs.Configs.ApiKeyRebrand)
	xlsx, err := excelize.OpenFile("./docs/"+configs.Configs.Filename)
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	sheet1Name := "Sheet4"

	rows := make([]M, 0)
	for i := 0; i < N; i++ {
		name, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("A%d", i+1))
		phone, _ := xlsx.GetCellValue(sheet1Name, fmt.Sprintf("B%d", i+1))

		token,_ := repository.GetLoginToken(phone)

		row := M{
			"Name":  name ,
			"Phone": phone,
			"Url": token.URI + "/start?img=" + token.AccessToken,
		}
		rows = append(rows, row)
		log.Println("login success: ",i)
	}

	for i := 0; i < N; i++ {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+1), rows[i]["Url"])
	}

	xlsx.SaveAs("./docs/"+configs.Configs.Filename)
	xlsx.Close()
}
