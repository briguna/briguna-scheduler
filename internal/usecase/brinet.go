package usecase

type BrinetUseCase interface {
	CheckOutStanding(string) (string, error)
	InquriryTagihan(string) (string, error)
	InquiryAft(string) (string, error)
	InquiryHold(string) (string, error)
}


