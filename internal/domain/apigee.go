package domain

import (
	"bitbucket.org/briguna/briguna-scheduler/internal/constant"
	"bitbucket.org/briguna/briguna-scheduler/pkg"
	"bitbucket.org/briguna/briguna-scheduler/pkg/rest"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

type AccesTokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

type AccessTokenResponse struct {
	RefreshTokenExpiresIn string   `json:"refresh_token_expires_in"`
	ApiProductList        string   `json:"api_product_list"`
	ApiProductListJson    []string `json:"api_product_list_json"`
	OrganizationName      string   `json:"organization_name"`
	TokenType             string   `json:"token_type"`
	IssuedAt              string   `json:"issued_at"`
	ClientId              string   `json:"client_id"`
	AccessToken           string   `json:"access_token"`
	ApplicationName       string   `json:"application_name"`
	ExpiresIn             string   `json:"expires_in"`
	RefreshCount          string   `json:"refresh_count"`
	Status                string   `json:"status"`
}

var AccessTokenInternal = ""
var AccessTokenEksternal string
var AccessTokenInternalDev string

func GetToken(apigee string) error {
	var baseUrl, endpoint, clientId, clientSecret string
	if apigee == constant.APIGEE_EKSTERNAL {
		baseUrl = pkg.Getenv("APIGEE_EKSTERNAL_URL")
		endpoint = "/oauth/client_credential/accesstoken"
		clientId = pkg.Getenv("APIGEE_EKSTERNAL_CLIENT_ID")
		clientSecret = pkg.Getenv("APIGEE_EKSTERNAL_SECRET_KEY")
	} else if apigee == constant.APIGEE_INTERNAL {
		baseUrl = pkg.Getenv("APIGEE_INTERNAL_URL")
		endpoint = "/oauth/client_credential/accesstoken"
		clientId = pkg.Getenv("APIGEE_INTERNAL_CLIENT_ID")
		clientSecret = pkg.Getenv("APIGEE_INTERNAL_SECRET_KEY")
	}

	url := baseUrl + endpoint
	Headers := make(map[string]string)
	Headers["Content-Type"] = "application/x-www-form-urlencoded"
	mapBody := map[string]string{
		"client_id":     clientId,
		"client_secret": clientSecret,
	}
	mapJson, _ := json.Marshal(mapBody)
	var Body = []byte(string(mapJson))
	queryParams := make(map[string]string)
	queryParams["grant_type"] = "client_credentials"
	method := rest.POST
	request := rest.Request{
		Method:      method,
		URL:         url,
		Headers:     Headers,
		QueryParams: queryParams,
		Body:        Body,
	}
	response, err := rest.SendRequestForm(request)
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(fmt.Sprintf("Error Get Response : %v", err.Error()))
		return err
	}
	log.Println(fmt.Sprintf("Response Apigee : status :%v  %v  ", response.StatusCode, string(body)))

	var jsonData AccessTokenResponse
	err = json.Unmarshal(body, &jsonData)
	if err != nil {
		log.Println(fmt.Sprintf("Error marshall : %v", err.Error()))
		return err
	}
	if apigee == constant.APIGEE_EKSTERNAL {
		AccessTokenEksternal = jsonData.AccessToken
	} else if apigee == constant.APIGEE_INTERNAL {
		AccessTokenInternal = jsonData.AccessToken
	}
	return nil
}

func GetSignature(path string, method string, body string, apigee string) (string, string) {
	var timestamp = getTimestamp()
	var token, clientSecret string
	if apigee == constant.APIGEE_EKSTERNAL {
		token = AccessTokenEksternal
		clientSecret = pkg.Getenv("APIGEE_EKSTERNAL_SECRET_KEY")
	} else if apigee == constant.APIGEE_INTERNAL {
		token = AccessTokenInternal
		clientSecret = pkg.Getenv("APIGEE_INTERNAL_SECRET_KEY")
	}

	payload := "path=" + path + "&verb=" + method + "&token=Bearer " + token + "&timestamp=" + timestamp + "&body=" + body
	log.Println("payload :" + payload)
	log.Println("secret key :" + clientSecret)

	var hmacSignature = computeHmac256(payload, clientSecret)
	return hmacSignature, timestamp
}

func computeHmac256(payload string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(payload))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func getTimestamp() string {
	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05.678Z")
	return timestamp
}

