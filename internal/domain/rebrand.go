package domain

type RebrandResponses struct {
	Id string`json:"id"`
	Title string`json:"title"`
	Slashtag string`json:"slashtag"`
	ShortUrl string`json:"shortUrl"`
}
