package domain

type TokenResponse struct{
	Status int `json:"status"`
	Contents TokenData `json:"data"`
}

type TokenData struct {
	AccessToken  string `json:"accessToken"`
	URI string `json:"uri"`
}







