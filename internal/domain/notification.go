package domain


type NotificationResponse struct {
	Body NotificationBody`json:"body"`
}

type NotificationBody struct {
	StatusCode string`json:"statusCode"`
	StatusDesc string`json:"statusDesc"`
}
