package domain

type OutstandingResponse struct{
	Status int `json:"status"`
	Data OutstandingData `json:"data"`
}

type OutstandingData struct {
	Response  string `json:"response"`
	Message string `json:"message"`
}







