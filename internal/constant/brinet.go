package constant

const (
	START                          = 100
	VERIFIED_OTP                   = 1
	SUBMIT_DATA_PRIBADI            = 2
	SUBMIT_DATA_KERABAT            = 3
	SUBMIT_DATA_KEUANGAN           = 4
	UPLOADED_KTP_NASABAH           = 5
	UPLOADED_NPWP_NASABAH          = 6
	UPLOADED_KTP                   = 7
	UPLOADED_IMAGE                 = 8
	UPLOADED_KK                    = 9
	PROCESS_KTP                    = 10
	DECLINED_KTP                   = 12
	PREESCRENING_PROCESS           = 13
	PREESCRENING_SUCCESS           = 14
	PREESCRENING_FAILED            = 15
	LOAN_ACCEPTED_BY_USER          = 16
	LOAN_DECLINED_BY_USER          = 17
	CREDIT_SCORING_FAILED          = 11
	ASLIRI_SUCCESS_NASABAH         = 18
	ASLIRI_FAILED_NASABAH          = 19 // screen take photo
	PRIVY_REGISTER_SUCCESS_NASABAH = 20
	PRIVY_REGISTER_FAILED_NASABAH  = 21 // screen take photo
	ASLIRI_SUCCESS                 = 22
	ASLIRI_FAILED                  = 23 // screen take photo
	PRIVY_REGISTER_SUCCESS         = 24
	PRIVY_REGISTER_FAILED          = 25 // screen take photo
	SIGN_DOCUMENT_NASABAH          = 26
	SIGN_DOCUMENT                  = 27
	DISBURSMENT_COMPLETED          = 28
)
